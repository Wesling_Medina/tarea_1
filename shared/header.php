<?php
    include_once ("php/arrayPages.php");
    $instance = new arrayPages();
    $instance->addPages([["name"=>"Page_1","href"=>"page_1.php"],["name"=>"Page_2","href"=>"page_2.php"],["name"=>"Page_3","href"=>"page_3.php"]]);
    $instance->addPages(array(["name"=>"Page_4","href"=>"page_4.php"],["name"=>"Page_5","href"=>"page_5.php"]),"More",true);
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <!--??, significa que si el title no esta definida use el string "Page"-->
	<title><?= $title ?? 'Page' ?></title>
	<link rel="stylesheet" type="text/css" href="./css/bulma.min.css">
	<link rel="stylesheet" type="text/css" href="./css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<nav class="navbar" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="/">
      <img src="./imgs/logo.png" >
    </a>

    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>

  <div id="navbarBasicExample" class="navbar-menu">
    <div class="navbar-start">
        <?php
            foreach ($instance->returnArray() as $pages){
                foreach ($pages as $key=>$page){
                    if($key=="false"){
                        while (list($clave, $valor) = each($page)){
                            $nombre = $valor["name"];
                            $dir = $valor["href"];
                            echo "<a class='navbar-item' href='$dir'>";
                                echo  $nombre;
                            echo "</a>";
                        }
                    }else{
                        echo "<div class='navbar-item has-dropdown is-hoverable'>";
                            echo  "<a class='navbar-link''>";
                                echo  $key;
                            echo "</a>";
                            echo "<div class='navbar-dropdown'>";
                                while (list($clave, $valor) = each($page)) {
                                    $href = $valor["href"];
                                    echo "<a class='navbar-item' href='$href'>";
                                        echo  $valor["name"];
                                    echo "</a>";
                                }
                            echo "</div>";
                        echo "</div>";
                    }
                }
            }
        ?>
    </div>
    <div class="navbar-end">
      <div class="navbar-item">
        <div class="buttons">
          <a class="button is-primary">
            <strong>Sign up</strong>
          </a>
          <a class="button is-light">
            Log in
          </a>
        </div>
      </div>
    </div>
  </div>
</nav>