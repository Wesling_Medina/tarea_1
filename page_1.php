<?php require_once './shared/header.php' ?>
<?php
    include_once ("./php/Person.php");
    $instance = new Person();
?>

    <section class="section">
    <div class="container">
      <h1 class="title">Person Resume</h1>
      <form action="">
          <div class="field">
          <label for="name" class="label">Name:</label>
              <div class="control">
                  <input class="input" type="text" name="name" value="<?= $instance->getName()?>"/>
              </div>
          </div>
          <div class="field">
          <label for="phone" class="label">Phone:
              <input class="input" type="tel" name="phone" value="<?= $instance->getPhone()?>"/>
          </label>
          </div>
          <div class="field">
          <label for="sex" class="label">Sex:
              <input class="input" type="text" name="sex" value="<?= $instance->getSex()?>"/>
          </label></div>
          <div class="field has-addons">
              <div class="control is-expanded">
                  <div class="select is-fullwidth">
                      <label for="select">Select:
                          <select name="select" multiple>
                              <?php
                              foreach ($instance->getHobbies() as $hobbie){
                                  echo "<option value='$hobbie'>$hobbie</option>";
                              }
                              ?>
                          </select>
                      </label>
                  </div>
              </div>
          </div>
      </form>
    </div>
</section>
<?php require_once './shared/footer.php' ?>