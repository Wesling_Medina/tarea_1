<?php

class arrayPages{
    private $arrayPages;
    public function __construct()
    {
        $this->arrayPages = [];
    }

    /**
     * @param string $name -> Use this attributte for naming a nested tab
     * @param array $arrayNested ->Add your tabs here
     * @param bool $isNested -> If you don't add this attribute or set it to true the program take the data as a no nested tab
     */
    public function addPages($arrayNested,$name = "",$isNested = false){
        if(!$isNested){
            array_push($this->arrayPages,array("false"=>$arrayNested));
        }else{
            array_push($this->arrayPages,array($name =>$arrayNested));
        }
    }

    public function returnArray(){
        return $this->arrayPages;
    }
}

