<?php

    class Person{
        private $name;
        private $phone;
        private $sex;
        private $hobbies;
        public function __construct()
        {
            $this->name = "John Doe";
            $this->phone = "63309923";
            $this->sex = "Male";
            $this->hobbies = ["Soccer","Watch T.V","Basketball","Volleyball","Baseball","Tennis"];
        }

        /**
         * @return string
         */
        public function getName()
        {
            return $this->name;
        }

        /**
         * @param string $name
         */
        public function setName($name)
        {
            $this->name = $name;
        }

        /**
         * @return string
         */
        public function getPhone()
        {
            return $this->phone;
        }

        /**
         * @param string $phone
         */
        public function setPhone($phone)
        {
            $this->phone = $phone;
        }

        /**
         * @return string
         */
        public function getSex()
        {
            return $this->sex;
        }

        /**
         * @param string $sex
         */
        public function setSex($sex)
        {
            $this->sex = $sex;
        }

        /**
         * @return array
         */
        public function getHobbies()
        {
            return $this->hobbies;
        }

        /**
         * @param array $hobbies
         */
        public function setHobbies($hobbies)
        {
            $this->hobbies = $hobbies;
        }


    }
?>